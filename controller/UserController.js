const User = require("../model/User");
const { validate, checkType } = require("../validate/validation");

exports.createUser = async (req, res, next) => {
  try {
    let data = req.body;
    const err = validate(data);
    const errCoordinate = checkType(data);
    if (err !== null) {
      return res.json({
        code: err.code,
        message: err.message,
      });
    }
    if (errCoordinate !== null) {
      return res.json({
        code: errCoordinate.code,
        message: errCoordinate.message,
      });
    }
    const array = req.body.Coordinate.split(":").map(Number);
    const user = User.create({
      FirstName: req.body.FirstName,
      LastName: req.body.LastName,
      Age: req.body.Age,
      Location: array,
      Coordinate: req.body.Coordinate,
    });
    if (user) {
      res.json({ code: 200, msg: "success", data: user });
    } else {
      res.json({ code: 400, msg: "Fail" });
    }
  } catch (err) {
    return res.json({ code: 500, msg: "Fail", error: err.message });
  }
};

exports.readUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.query.id });
    if (user) {
      res.json({ code: 200, msg: "success", data: user });
    } else {
      res.json({ code: 400, msg: "Fail" });
    }
  } catch (err) {
    return res.json({ code: 500, msg: "Fail", error: err.message });
  }
};

exports.updateUser = async (req, res, next) => {
  try {
    const user = await User.updateOne({ _id: req.params.id }, req.body);
    if (user) {
      res.json({ code: 200, msg: "success", data: req.body });
    } else {
      res.json({ code: 400, msg: "Fail", data: null });
    }
  } catch (err) {
    return res.json({ code: 500, msg: "Fail", error: err.message });
  }
};

exports.deleteUser = async (req, res, next) => {
  try {
    const user = await User.deleteOne({ _id: req.params.id });
    if (user) {
      res.json({ code: 200, msg: "success", data: req.params.id });
    } else {
      res.json({ code: 400, msg: "Fail", data: null });
    }
  } catch (err) {
    return res.json({ code: 500, msg: "Fail", error: err.message });
  }
};

exports.searchUser = async (req, res, next) => {
  try {
    const user = await User.find({
      $or: [
        { FirstName: { $regex: req.query.name } },
        { LastName: { $regex: req.query.name } },
      ],
    }).sort({ FirstName: -1 });

    if (user) {
      res.json({ code: 200, msg: "success", data: user });
    } else {
      res.json({ code: 400, msg: "Fail", data: null });
    }
  } catch (err) {
    return res.json({ code: 500, msg: "Fail", error: err.message });
  }
};

exports.located = async (req, res, next) => {
  try {
    const n = req.query.n ? req.query.n : null;

    const user = await User.find({})
      .where("_id")
      .ne(req.query.id)
      .sort({ Coordinate: 1 })
      .limit(n);

    if (user) {
      res.json({ code: 200, msg: "success", data: user });
    } else {
      res.json({ code: 400, msg: "Fail", data: null });
    }
  } catch (err) {
    return res.json({ code: 500, msg: "Fail", error: err.message });
  }
};
