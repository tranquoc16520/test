const validate = (data) => {
    if (!data.FirstName) {
        return {code: 300,  message: 'Không được để trống trường FirstName'}
    } 
    if (typeof data.FirstName !== "string") {
        return {code: 400,  message: 'FirstName phải là kiểu chuỗi'}
    } 
    if (!data.LastName) {
        return {code: 300,  message: 'Không được để trống trường LastName'}
    } 
    if (typeof data.LastName !== "string") {
        return {code: 400,  message: 'LastName phải là kiểu chuỗi'}
    } 
    if (!data.Age) {
        return {code: 300,  message: 'Không được để trống trường Age'}
    }
    if (!Number.isInteger(data.Age)) {
        return {code: 400,  message: 'Age phải là kiểu số'}
    } 
    if (!data.Coordinate) {
        return {code: 300,  message: 'Không được để trống trường Coordinate'}
    }
    if (typeof data.Coordinate !== "string") {
        return {code: 400,  message: 'LastName phải là kiểu chuỗi'}
    } 

    return null
}

const checkType = (data) => {
    const array = data.Coordinate.split(':').map(Number)
    let check = 0
    for (let i = 0; i < array.length; i++) {
        if (isNaN(array[i])) {
            check += 1
        }
    }
    if (data.Coordinate.length !== 7 || check !==0) {
        return {code: 300,  message: 'phải là định dạng xxx:yyy với x và y là số'} 
    }
    return null
}

module.exports = { validate, checkType}