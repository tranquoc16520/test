const express = require('express');
const router = express.Router();
const {createUser, readUser, updateUser, deleteUser, searchUser, located} = require('../controller/UserController')


router.post('/user/add', (req, res, next) => createUser(req, res, next))
router.get('/locate', (req, res, next) => located(req, res, next))
router.get('/user/read', (req, res, next) => readUser(req, res, next))
router.get('/user/search', (req, res, next) => searchUser(req, res, next))
router.put('/user/edit/:id', (req, res, next) => updateUser(req, res, next))
router.delete('/user/delete/:id', (req, res, next) => deleteUser(req, res, next))


module.exports = router;
