const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    FirstName: { type: String, required: false},
    LastName: { type: String, required: false},
    Age: { type: Number, required: false},
    Coordinate: { type: String, required: false}
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("users", UserSchema);
