const express = require('express')
const dotenv = require('dotenv')
const bodyParser = require('body-parser');
const userRouter = require('./router/user')
const db = require('./config/db')

const app = express()
// Config env
dotenv.config()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(
	bodyParser.urlencoded({
		extended: false,
	}),
);
// Connect DB
db.connect()
// Router
app.use('/', userRouter)

const port = process.env.PORT || 3000
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

